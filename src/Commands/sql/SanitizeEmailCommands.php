<?php

namespace Drupal\sanitize_pii\Commands\sql;

use Consolidation\AnnotatedCommand\CommandData;
use Drupal\Core\Database\Connection;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\Sql\DefaultTableMapping;
use Drupal\Core\Entity\Sql\SqlEntityStorageInterface;
use Drupal\sanitize_pii\Services\SanitizeService;
use Drush\Commands\DrushCommands;
use Drush\Drupal\Commands\sql\SanitizePluginInterface;
use Symfony\Component\Console\Input\InputInterface;

/**
 * Provides a drush sql:sanitize plugin for email fields.
 *
 * It overwrites key columns with fixed data.
 */
class SanitizeEmailCommands extends DrushCommands implements SanitizePluginInterface {

  /**
   * The sanitize service.
   *
   * @var \Drupal\sanitize_pii\Services\SanitizeService
   */
  protected $sanitizeService;

  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * Creates the telephone field sanitizer.
   *
   * @param \Drupal\sanitize_pii\Services\SanitizeService $sanitize_service
   *   The sanitize service.
   * @param \Drupal\Core\Database\Connection $database
   *   The database connection.
   */
  public function __construct(SanitizeService $sanitize_service, Connection $database) {
    parent::__construct();
    $this->sanitizeService = $sanitize_service;
    $this->database = $database;
  }

  /**
   * {@inheritdoc}
   *
   * Overwrites key email field columns.
   *
   * @hook post-command sql-sanitize
   */
  public function sanitize($result, CommandData $commandData) {
    $field_type = 'email';
    $sanitize_data = $this->sanitizeService->getDataToSanitize($field_type);
    foreach ($sanitize_data as $table => $fields) {
      // Ignore users_field_data table, emails are already sanitized by drush core.
      if ($table === 'users_field_data') {
        continue;
      }
      foreach ($fields as $field) {
        $this->sanitizeColumn($table, $field);
        $this->logger()->success(dt('Sanitized @field_type fields in @table.', [
          '@table' => $table,
          '@field_type' => $field_type,
        ]));
      }
    }
  }

  /**
   * {@inheritdoc}
   *
   * @hook on-event sql-sanitize-confirms
   */
  public function messages(&$messages, InputInterface $input) {
    $messages[] = dt('Sanitize email fields.');
  }

  /**
   * Update a table column with sanitized data.
   *
   * @param string $table
   *   The table name to update.
   * @param string $column
   *   The database column name to update.
   */
  protected function sanitizeColumn(string $table, string $column) {
    $not_empty = $this->database->condition('AND')
      ->condition($column, NULL, 'IS NOT NULL')
      ->condition($column, '', '<>');
    $this->database->update($table)
      ->condition($not_empty)
      ->fields([$column => 'user@localhost.localdomain'])
      ->execute();
  }

}

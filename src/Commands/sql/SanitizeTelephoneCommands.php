<?php

namespace Drupal\sanitize_pii\Commands\sql;

use Consolidation\AnnotatedCommand\CommandData;
use Drupal\Core\Database\Connection;
use Drupal\Core\Extension\ModuleHandler;
use Drupal\sanitize_pii\Services\SanitizeService;
use Drush\Commands\DrushCommands;
use Drush\Drupal\Commands\sql\SanitizePluginInterface;
use Symfony\Component\Console\Input\InputInterface;

/**
 * Provides a drush sql:sanitize plugin for telephone number fields.
 *
 * It overwrites key columns with fixed data.
 */
class SanitizeTelephoneCommands extends DrushCommands implements SanitizePluginInterface {

  /**
   * The sanitize service.
   *
   * @var \Drupal\sanitize_pii\Services\SanitizeService
   */
  protected $sanitizeService;

  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandler
   */
  protected $moduleHandler;

  /**
   * Creates the telephone field sanitizer.
   *
   * @param \Drupal\sanitize_pii\Services\SanitizeService $sanitize_service
   *   The sanitize service.
   * @param \Drupal\Core\Database\Connection $database
   *   The database connection.
   * @param \Drupal\Core\Extension\ModuleHandler $module_handler
   *   The module handler.
   */
  public function __construct(SanitizeService $sanitize_service, Connection $database, ModuleHandler $module_handler) {
    parent::__construct();
    $this->sanitizeService = $sanitize_service;
    $this->database = $database;
    $this->moduleHandler = $module_handler;
  }

  /**
   * {@inheritdoc}
   *
   * Overwrites key telephone field columns.
   *
   * @hook post-command sql-sanitize
   */
  public function sanitize($result, CommandData $commandData) {
    if (!$this->moduleHandler->moduleExists('telephone')) {
      $this->logger()->success(dt('The telephone module is not enabled so there are no telephone fields to sanitize.'));
      return;
    }

    $field_type = 'telephone';
    $sanitize_data = $this->sanitizeService->getDataToSanitize($field_type);
    foreach ($sanitize_data as $table => $fields) {
      foreach ($fields as $field) {
        $this->sanitizeColumn($table, $field);
        $this->logger()->success(dt('Sanitized @field_type fields in @table.', [
          '@table' => $table,
          '@field_type' => $field_type,
        ]));
      }
    }
  }

  /**
   * {@inheritdoc}
   *
   * @hook on-event sql-sanitize-confirms
   */
  public function messages(&$messages, InputInterface $input) {
    $messages[] = dt('Sanitize telephone fields.');
  }

  /**
   * Update a table column with sanitized data.
   *
   * @param string $table
   *   The table name to update.
   * @param string $column
   *   The database column name to update.
   */
  protected function sanitizeColumn(string $table, string $column) {
    $not_empty = $this->database->condition('AND')
      ->condition($column, NULL, 'IS NOT NULL')
      ->condition($column, '', '<>');
    $this->database->update($table)
      ->condition($not_empty)
      ->fields([$column => '0000000000'])
      ->execute();
  }

}

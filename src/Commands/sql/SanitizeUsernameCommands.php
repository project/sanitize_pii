<?php

namespace Drupal\sanitize_pii\Commands\sql;

use Consolidation\AnnotatedCommand\CommandData;
use Drupal\Core\Database\Connection;
use Drupal\sanitize_pii\Services\SanitizeService;
use Drush\Commands\DrushCommands;
use Symfony\Component\Console\Input\InputInterface;
use Drush\Drupal\Commands\sql\SanitizePluginInterface;

/**
 * A sql-sanitize plugin.
 */
class SanitizeUsernameCommands extends DrushCommands implements SanitizePluginInterface {

  /**
   * Provides a drush sql:sanitize plugin for user name field.
   *
   * @var \Drupal\sanitize_pii\Services\SanitizeService
   */
  protected $sanitizeService;

  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * Creates the user name field sanitizer.
   *
   * @param \Drupal\sanitize_pii\Services\SanitizeService $sanitize_service
   *   The sanitize service.
   * @param \Drupal\Core\Database\Connection $database
   *   The database connection.
   */
  public function __construct(SanitizeService $sanitize_service, Connection $database) {
    parent::__construct();
    $this->sanitizeService = $sanitize_service;
    $this->database = $database;
  }

  /**
   * Sanitize user names.
   *
   * @hook post-command sql-sanitize
   *
   * @inheritdoc
   */
  public function sanitize($result, CommandData $commandData) {
    $this->database->update('users_field_data')
      ->condition('uid', 1, '>')
      ->expression('name', "CONCAT('user+', uid)")
      ->execute();
    $storage = $this->sanitizeService->getEntityTypeStorage('user');
    $storage->resetCache();
    $this->logger()->success(dt('Sanitized user names.'));
  }

  /**
   * {@inheritdoc}
   *
   * @hook on-event sql-sanitize-confirms
   */
  public function messages(&$messages, InputInterface $input) {
    $messages[] = dt('Sanitize user names.');
  }

}

<?php

namespace Drupal\sanitize_pii\Services;

use Consolidation\AnnotatedCommand\CommandData;
use Drupal\Core\Database\Connection;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\ContentEntityNullStorage;
use Drupal\Core\Entity\Sql\DefaultTableMapping;
use Drupal\Core\Entity\Sql\SqlEntityStorageInterface;
use Drupal\Core\Entity\Sql\TableMappingInterface;

/**
 * Helper functions for sanitizing PII in the database.
 */
class SanitizeService {

  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * The entity field manager.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $entityFieldManager;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Creates the sanitizer service.
   *
   * @param \Drupal\Core\Database\Connection $database
   *   The database connection.
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entity_field_manager
   *   The entity field manager.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(Connection $database, EntityFieldManagerInterface $entity_field_manager, EntityTypeManagerInterface $entity_type_manager) {
    $this->database = $database;
    $this->entityFieldManager = $entity_field_manager;
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * Gets the tables and fields that need to be sanitized for the field type.
   *
   * @param string $field_type
   *   The field type.
   *
   * @return array
   *   Tables and fields to sanitize.
   */
  public function getDataToSanitize(string $field_type) {
    $sanitize_data = [];
    $fields = $this->getSanitizeFields($field_type);
    foreach ($fields as $entity_type_id => $field_names) {
      $mapping = $this->getTableMapping($entity_type_id);
      if (empty($mapping)) {
        continue;
      }

      foreach ($field_names as $field_name) {
        $column_names = $mapping->getColumnNames($field_name);
        foreach ($this->getAllFieldTableNames($entity_type_id, $field_name, $mapping) as $field_table) {
          $sanitize_data[$field_table][] = $column_names['value'];
        }
      }
    }
    return $sanitize_data;
  }

  /**
   * Returns the names of fields to be sanitized.
   *
   * @param string $field_type
   *   The type of field.
   *
   * @return string[][]
   *   An associative array keyed by entity type ID of arrays of field names.
   */
  public function getSanitizeFields(string $field_type) {
    return array_map(function (array $field_bundle_data) {
      return array_keys($field_bundle_data);
    }, $this->entityFieldManager->getFieldMapByFieldType($field_type));
  }

  /**
   * Gets storage for the entity type.
   *
   * @param string $entity_type_id
   *   The entity type ID for this storage.
   *
   * @returns \Drupal\Core\Entity\EntityStorageInterface
   *   A storage instance.
   */
  public function getEntityTypeStorage(string $entity_type_id) {
    return $this->entityTypeManager->getStorage($entity_type_id);
  }

  /**
   * Gets all the table names in which an entity field is stored.
   *
   * When there's no need to support Drupal 8 switch to
   * Drupal\Core\Entity\Sql\TableMappingInterface::getAllFieldTableNames().
   *
   * @param string $entity_type_id
   *   The ID of the entity type the field's attached to.
   * @param string $field_name
   *   The name of the entity field to return the tables names for.
   * @param \Drupal\Core\Entity\Sql\TableMappingInterface $mapping
   *   The table mapping object.
   *
   * @return string[]
   *   An indexed array of table names.
   */
  public function getAllFieldTableNames(string $entity_type_id, string $field_name, TableMappingInterface $mapping) {
    $entity_type = $this->entityTypeManager->getDefinition($entity_type_id);
    $definitions = $this->entityFieldManager->getFieldStorageDefinitions($entity_type_id);
    $definition = $definitions[$field_name];

    $tables = [$mapping->getFieldTableName($field_name)];
    // Ensure we get both the main table and revision table where appropriate.
    if ($entity_type->isRevisionable()) {
      if ($mapping->requiresDedicatedTableStorage($definition)) {
        $tables[] = $mapping->getDedicatedRevisionTableName($definition);
      }
      else {
        $tables[] = $entity_type->getRevisionDataTable();
      }
    }
    return $tables;
  }

  /**
   * Returns the table mapping for a given entity type backed by SQL storage.
   *
   * @param string $entity_type_id
   *   The entity type ID to get the table mapping for.
   *
   * @return \Drupal\Core\Entity\Sql\TableMappingInterface
   *   The table mapping object.
   *
   * @throws \RuntimeException
   *   If the entity storage doesn't implement \Drupal\Core\Entity\Sql\SqlEntityStorageInterface.
   */
  public function getTableMapping($entity_type_id) {
    $storage = $this->entityTypeManager->getStorage($entity_type_id);
    if ($storage instanceof ContentEntityNullStorage) {
      return NULL;
    }

    if (!$storage instanceof SqlEntityStorageInterface) {
      $context = ['!entity_type_id' => $entity_type_id];
      throw new \RuntimeException(dt("Unable to get table mapping from !entity_type_id entity storage service.", $context));
    }
    $mapping = $storage->getTableMapping();
    if (!$mapping instanceof DefaultTableMapping) {
      throw new \RuntimeException(dt("Table mapping class must be instance of DefaultTableMapping"));
    }
    return $mapping;
  }

}

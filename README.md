INTRODUCTION
---------------------

This module sanitizes all field types from Drupal core that could contain Personal Identifiable Information.

FAQ
---
Q: Drush core already sanitizes email addresses in the `users_field_data` table; should the functionality in this module be added into Drush core?

A: The advantage of doing this in Drupal is that we can use Drupal functions to dynamically figure out the tables and fields where these field types are stored.

Q: Should user names by sanitized by Drush core?

A: Maintainers of Drush core have said [no](https://github.com/drush-ops/drush/issues/4609).

MAINTAINERS
-----------

Current maintainers:
* TKWW Paper Team

Special thanks to [AndyF](https://www.drupal.org/u/andyf), whose [address sanitization](https://www.drupal.org/project/address/issues/3189074) work was heavily borrowed for this module.
